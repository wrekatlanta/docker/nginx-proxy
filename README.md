# WREK nginx-proxy
Simple Docker setup for making docker containers reachable

## Setup
1. Make a copy of the example env file: `cp example.env .env`
1. Replace the values in .env
1. Run `docker-compose up -d` to start the container

## Updating
Run `docker-compose pull` and then `docker-compose up -d` to update and re-create the container
